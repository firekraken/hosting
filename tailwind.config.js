module.exports = {
  prefix: "",
  purge: {
    content: ["./src/**/*.{html,ts}"],
  },
  darkMode: "media",
  theme: {
    extend: {
      colors: {
        primary: "#51be98",
        primaryDark: "#166a66",
      },
      screens: {
        "lg-down": { max: "1023px" },
      },
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
