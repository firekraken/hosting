import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NumberDataset } from '../dashboard/number/number.component';

@Component({
  selector: 'fk-ath',
  templateUrl: './ath.component.html',
  styleUrls: ['./ath.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AthComponent {
  readonly ath: Observable<NumberDataset>;

  constructor(firestore: AngularFirestore) {
    this.ath = firestore
      .doc('data/ath')
      .valueChanges()
      .pipe(
        map((ath: any) => ({
          value: ath.total,
          timestamp: ath.timestamp.toDate(),
        }))
      );
  }
}
