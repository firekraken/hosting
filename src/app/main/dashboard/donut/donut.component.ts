import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Observable } from 'rxjs';

export interface DonutDataset {
  labels: string[];
  data: number[];
  colors: string[];
}

@Component({
  selector: 'fk-donut',
  templateUrl: './donut.component.html',
  styleUrls: ['./donut.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DonutComponent {
  @Input() header?: string;
  @Input() dataset?: Observable<DonutDataset | null>;
}
