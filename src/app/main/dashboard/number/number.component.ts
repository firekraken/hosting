import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { Observable } from 'rxjs';

export interface NumberDataset {
  value: number;
  timestamp: Date;
}

@Component({
  selector: 'fk-number',
  templateUrl: './number.component.html',
  styleUrls: ['./number.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NumberComponent {
  @Input() header?: string;
  @Input() dataset?: Observable<NumberDataset>;
}
