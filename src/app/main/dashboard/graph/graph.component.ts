import { Component, EventEmitter, Input, Output } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Observable } from 'rxjs';

export interface GraphDataset {
  data: ChartDataSets[];
  labels: string[];
  colors: any[];
}

export interface Interval {
  label: string;
  value: number;
  interval: number;
}

export const intervals: Interval[] = [
  {
    label: '1w',
    value: 120,
    interval: 7 * 48,
  },
  {
    label: '1d',
    value: 30,
    interval: 48,
  },
  {
    label: '4h',
    value: 7,
    interval: 8,
  },
  {
    label: '1h',
    value: 3,
    interval: 2,
  },
  {
    label: '30m',
    value: 1,
    interval: 1,
  },
];

@Component({
  selector: 'fk-graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.css'],
})
export class GraphComponent {
  @Input() header?: string;
  @Input() dataset?: Observable<GraphDataset>;

  @Output() daysChanged = new EventEmitter<Interval>();

  options: ChartOptions = {
    legend: {
      position: 'right',
      labels: {
        usePointStyle: true,
      },
    },
    responsive: true,
    maintainAspectRatio: false,
    elements: {
      line: {
        borderWidth: 1,
      },
      point: {
        radius: 0,
      },
    },
    scales: {
      xAxes: [
        {
          display: false,
        },
      ],
    },
    hover: {
      intersect: false,
      mode: 'label',
    },
    tooltips: {
      mode: 'label',
      intersect: false,
    },
  };

  private _days: Interval | undefined;
  readonly allowedDays = intervals;

  @Input()
  get days(): Interval | undefined {
    return this._days;
  }

  set days(days: Interval | undefined) {
    this._days = days;
    if (days) {
      this.daysChanged.emit(days);
    }
  }
}
