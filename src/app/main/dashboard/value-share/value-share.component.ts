import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { assetColor, assetName } from '../../assets';
import { DonutDataset } from '../donut/donut.component';

type Data = { shares: Shares };
type Shares = { code: string; value: number }[];

@Component({
  selector: 'fk-value-share',
  templateUrl: './value-share.component.html',
  styleUrls: ['./value-share.component.css'],
})
export class ValueShareComponent {
  readonly shares$: Observable<DonutDataset | null>;

  constructor(firestore: AngularFirestore) {
    this.shares$ = firestore
      .doc<Data>('data/valueShares')
      .valueChanges()
      .pipe(
        map((data: Data | undefined) => data?.shares),
        map((shares: Shares | undefined) => {
          if (!shares) {
            return null;
          }

          return {
            labels: shares.map((s) => assetName(s.code)),
            data: shares.map((s) => s.value),
            colors: shares.map((s) => assetColor(s.code)),
          };
        })
      );
  }
}
