import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { assetColor, assetName } from '../../assets';
import { DonutDataset } from '../donut/donut.component';

type Data = { shares: Shares };
type Shares = { code: string; quantity: number }[];

@Component({
  selector: 'fk-quantity-share',
  templateUrl: './quantity-share.component.html',
  styleUrls: ['./quantity-share.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class QuantityShareComponent {
  readonly shares$: Observable<DonutDataset | null>;

  constructor(firestore: AngularFirestore) {
    this.shares$ = firestore
      .doc<Data>('data/quantityShares')
      .valueChanges()
      .pipe(
        map((data: Data | undefined) => data?.shares),
        map((shares: Shares | undefined) => {
          if (!shares) {
            return null;
          }

          return {
            labels: shares.map((s) => assetName(s.code)),
            data: shares.map((s) => s.quantity),
            colors: shares.map((s) => assetColor(s.code)),
          };
        })
      );
  }
}
