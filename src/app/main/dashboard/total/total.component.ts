import { Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { NumberDataset } from '../number/number.component';

@Component({
  selector: 'fk-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css'],
})
export class TotalComponent {
  readonly total: Observable<NumberDataset>;

  constructor(firestore: AngularFirestore) {
    this.total = firestore
      .doc('data/current')
      .valueChanges()
      .pipe(
        map((total: any) => ({
          value: total.total,
          timestamp: total.timestamp.toDate(),
        }))
      );
  }
}
