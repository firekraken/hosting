import { ChangeDetectionStrategy, Component } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { assetColor, assetName, formatDate } from '../../assets';
import Snapshot from '../../snapshots.interface';
import { GraphDataset, Interval, intervals } from '../graph/graph.component';

@Component({
  selector: 'fk-asset-value',
  templateUrl: './asset-value.component.html',
  styleUrls: ['./asset-value.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AssetValueComponent {
  readonly initialInterval = intervals[2];

  dataset: Observable<GraphDataset> | undefined;

  constructor(private readonly firestore: AngularFirestore) {
    this.dataset = this.refreshDatasets(this.initialInterval);
  }

  refreshDatasets(interval: Interval): Observable<GraphDataset> {
    return this.firestore
      .collection<Snapshot>('snapshots', (ref) =>
        ref.orderBy('timestamp', 'desc').limit(interval.value * 48)
      )
      .valueChanges()
      .pipe(
        debounceTime(500),
        map((snapshots) =>
          snapshots.filter((_, i) => i % interval.interval === 0)
        ),
        map((snapshots) => [...snapshots].reverse()),
        map((snapshots) => {
          const labels = snapshots.map((s) => formatDate(s.timestamp));

          const data = snapshots
            .reduce((agg: any, current, idx) => {
              current.assets.forEach((ass) => {
                if (!agg.some((a: any) => a.label === ass.code)) {
                  agg.push({ data: Array(labels.length), label: ass.code });
                }
                const total = ass.value * ass.quantity;
                agg.find((a: any) => a.label === ass.code).data[idx] =
                  total < 5 ? null : ass.value.toFixed(2);
              });
              return agg;
            }, [])
            .filter((d: any) => !d.data.every((x: any) => !x))
            .map((d: any) => {
              d.label = assetName(d.label);
              return d;
            })
            .sort((a: any, b: any) => a.label.localeCompare(b.label));

          const colors = data.map((d: any) => assetColor(d.label));
          return {
            labels,
            data,
            colors: colors.map((color: string) => ({
              borderColor: color,
              pointHoverBackgroundColor: color,
              pointBackgroundColor: color,
              pointHoverRadius: 2,
              backgroundColor: 'transparent',
            })),
          };
        })
      );
  }
}
