import { Asset } from './snapshots.interface';

const assetList = [
  {
    code: 'ACA',
    color: '#df4f61',
    name: 'Acala',
  },
  {
    code: 'DOT',
    color: '#e6007a',
    name: 'Polkadot',
  },
  {
    code: 'GLMR',
    color: '#53ccc9',
    name: 'Glimmer',
  },
  {
    code: 'GRT',
    color: '#6f4cff',
    name: 'The Graph',
  },
  {
    code: 'KAR',
    color: '#e40c5b',
    name: 'Karura',
  },
  {
    code: 'KSM',
    color: '#e6007a',
    name: 'Kusama',
  },
  {
    code: 'LINK',
    color: '#2A5ADA',
    name: 'Chainlink',
  },
  {
    code: 'MINA',
    color: '#ff603b',
    name: 'Mina',
  },
  {
    code: 'OCEAN',
    color: '#ff4092',
    name: 'Ocean',
  },
  {
    code: 'SDN',
    color: '#a12bbe',
    name: 'Shiden',
  },
  {
    code: 'SOL',
    color: '#00ffbd',
    name: 'Solana',
  },
  {
    code: 'XLM',
    color: '#000000',
    name: 'Stellar Lumen',
  },
];

export const assetColor = (code: string) =>
  assetList.find((asset) => asset.code === code || asset.name === code)
    ?.color || '#166a66';

export const assetName = (code: string) =>
  assetList.find((asset) => asset.code === code)?.name || code;

export const filteredAssets = (assets: Asset[]) =>
  assets.filter((a) => a.quantity * a.value > 5);

export const formatDate = (timestamp: any) => {
  const date = timestamp.toDate();
  const day = date.getUTCDate().toString().padStart(2, '0');
  const month = (date.getUTCMonth() + 1).toString().padStart(2, '0');
  const year = date.getUTCFullYear().toString().padStart(2, '0');
  const hour = date.getHours().toString().padStart(2, '0');
  const minutes = date.getMinutes().toString().padStart(2, '0');

  return `${day}/${month}/${year} ${hour}:${minutes}`;
};
