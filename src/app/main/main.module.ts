import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsModule } from 'ng2-charts';

import { DashboardComponent } from './dashboard/dashboard.component';
import { MainRoutingModule } from './main-routing.module';
import { QuantityShareComponent } from './dashboard/quantity-share/quantity-share.component';
import { SharedModule } from '../shared/shared.module';
import { AthComponent } from './ath/ath.component';
import { TotalComponent } from './dashboard/total/total.component';
import { ValueShareComponent } from './dashboard/value-share/value-share.component';
import { AssetValueComponent } from './dashboard/asset-value/asset-value.component';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { BalanceComponent } from './dashboard/balance/balance.component';
import { GraphComponent } from './dashboard/graph/graph.component';
import { DonutComponent } from './dashboard/donut/donut.component';
import { NumberComponent } from './dashboard/number/number.component';
@NgModule({
  declarations: [
    AssetValueComponent,
    AthComponent,
    BalanceComponent,
    DashboardComponent,
    GraphComponent,
    QuantityShareComponent,
    TotalComponent,
    ValueShareComponent,
    DonutComponent,
    NumberComponent,
  ],
  imports: [
    CommonModule,
    MainRoutingModule,
    SharedModule,
    ChartsModule,
    AngularFirestoreModule.enablePersistence(),
  ],
})
export class MainModule {}
