import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'fk-logo',
  templateUrl: './logo.component.html',
  styleUrls: ['./logo.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LogoComponent {
  @Input() cls = '';
}
