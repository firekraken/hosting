import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LogoComponent } from './logo/logo.component';
import { HeaderComponent } from './header/header.component';
import { SpinnerComponent } from './spinner/spinner.component';

@NgModule({
  declarations: [HeaderComponent, LogoComponent, SpinnerComponent],
  exports: [HeaderComponent, LogoComponent, SpinnerComponent],
  imports: [CommonModule],
})
export class SharedModule {}
