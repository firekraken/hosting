import { Component } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';
import firebase from 'firebase/app';

@Component({
  selector: 'fk-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent {
  constructor(public readonly auth: AngularFireAuth, router: Router) {
    auth.authState.subscribe((user) => {
      if (user) {
        router.navigate(['/']);
      }
    });
  }

  login(): void {
    const provider = new firebase.auth.GoogleAuthProvider();
    provider.setCustomParameters({ prompt: 'select_account' });
    this.auth.signInWithPopup(provider);
  }

  logout(): void {
    this.auth.signOut();
  }
}
