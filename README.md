# Firekraken - Frontend

FireKraken (mix between Firebase and Kraken) is a dashboard to follow your cryptocurrency investment made on Kraken.

# How does it work ?

Every 30 minutes, a Firebase function is run. This function will fetch the latest data from your Kraken account regarding your balance and the value of the coins you own.
Those data will be added into a Firestore database after some processing (computing total values, highest value, ..).

This project is only the frontend, which only shows the data from Firestore. The server-side project can be found here : [FireKraken/Function](https://gitlab.com/firekraken/functions)

# How do I use it ?

To use this, you obviously need the Firebase project running (check [FireKraken/Function](https://gitlab.com/firekraken/functions) to know how to run it).

Once your backend is running, you have a couple things to do :

1. Edit `.firebaserc` with your project
1. Update `assetList` in `src/app/main/assets.ts` with the tokens you own (some data is already set as an example)
1. Install the dependencies: `npm i`
1. Test everything locally: `npm start`

Once you're ready to deploy

1. Get your Firebase JSON file and put everything in `src/firebase.config.ts`
1. Deploy it: `npm run deploy`

# Main dependencies

This project uses

- Angular 12
- [ng2-charts](https://github.com/valor-software/ng2-charts)
- [Angular Fire](https://github.com/angular/angularfire)
- [Tailwind CSS](https://tailwindcss.com/)
